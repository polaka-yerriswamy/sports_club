package com.ty.Sports_Club.testCode;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.ty.Sports_Club.Controller.EventPriceController;
import com.ty.Sports_Club.Dao.EventPriceDao;
import com.ty.Sports_Club.Dto.EventPrice;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EventPriceTesting {
	
	@Autowired
	private EventPriceController eventPriceController;

	@MockBean
	private EventPriceDao dao;

	@Test
	public void saveEventPriceTest() {

		EventPrice eventPrice = new EventPrice(1, 2.33, 2.33, 23.32, null);

		when(dao.saveEventPrice(eventPrice)).thenReturn(eventPrice);
		assertEquals(eventPrice, eventPriceController.saveEventPrice(eventPrice).getBody().getData());
	}
	
	@Test
	public void getUserById() {
		EventPrice eventPrice = new EventPrice(1, 2.33, 2.33, 23.32, null);
		when(dao.getEventPriceById(1)).thenReturn(eventPrice);
		assertEquals(eventPrice, eventPriceController.getEventPriceById(1).getBody().getData());
	}

	@Test
	public void getAllEventPrices() {
		List<EventPrice> eventPrices = new ArrayList<EventPrice>();
		EventPrice eventPrice = new EventPrice(1, 2.33, 2.33, 23.32, null);
		EventPrice eventPrice1 = new EventPrice(1, 2.33, 2.33, 23.32, null);
		EventPrice eventPrice2 = new EventPrice(1, 2.33, 2.33, 23.32, null);
		eventPrices.add(eventPrice1);
		eventPrices.add(eventPrice2);
		eventPrices.add(eventPrice);
		when(dao.getAllEventPrices()).thenReturn(eventPrices);
		assertEquals(3, eventPriceController.getAllEventPrice().getBody().getData().size());
	}

	@Test
	public void updatEventPrices() {
		EventPrice eventPrice = new EventPrice(1, 2.33, 2.33, 23.32, null);
		when(dao.updateEventPrice(1, eventPrice)).thenReturn(eventPrice);
		assertEquals(eventPrice, eventPriceController.updateEventPrice(1, eventPrice).getBody().getData());
	}
	
	@Test
	public void deleteEventPriceTest() {
		boolean b = true;
		String del="Event Price deleted";
		when(dao.deleteEventPrice(1)).thenReturn(b);
		assertEquals(del, eventPriceController.deleteUser(1).getBody().getData());
	}


}
