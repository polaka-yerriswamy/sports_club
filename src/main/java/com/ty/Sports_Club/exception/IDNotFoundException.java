package com.ty.Sports_Club.exception;

@SuppressWarnings("serial")
public class IDNotFoundException extends RuntimeException{
		
		String message="user not found";
		public IDNotFoundException() {
			
		}
		
		public IDNotFoundException(String message) {
			this.message = message;
		}
		
		@Override
		public String getMessage() {
			return message;
		}

}
