package com.ty.Sports_Club.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.ty.Sports_Club.Utils.ResponseStructure;

@ControllerAdvice
public class ExceptionHandeller {
	@ExceptionHandler(Exception.class)
	public ResponseEntity<ResponseStructure<String>> handelIt(Exception exception){
		ResponseStructure<String> structure=new ResponseStructure<String>();
		structure.setStatuscode(HttpStatus.NOT_FOUND.value());
		structure.setMessage(exception.getMessage());
		structure.setData("these the exception handeler");
		ResponseEntity<ResponseStructure<String>> entity;
		return entity=new ResponseEntity<ResponseStructure<String>>(structure, HttpStatus.NOT_FOUND);
	}

}
