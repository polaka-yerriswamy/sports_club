package com.ty.Sports_Club.Repositry;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ty.Sports_Club.Dto.Events;

public interface EventsRepository extends JpaRepository<Events, Integer>{

}
