package com.ty.Sports_Club.Repositry;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ty.Sports_Club.Dto.EventPrice;

public interface EventPriceRepository extends JpaRepository<EventPrice, Integer>{

}
