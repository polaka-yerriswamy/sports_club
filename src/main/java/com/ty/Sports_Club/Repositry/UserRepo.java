package com.ty.Sports_Club.Repositry;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ty.Sports_Club.Dto.User;

public interface UserRepo extends  JpaRepository<User, Integer> {

}
