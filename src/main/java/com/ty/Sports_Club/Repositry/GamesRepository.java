package com.ty.Sports_Club.Repositry;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ty.Sports_Club.Dto.Games;

public interface GamesRepository extends JpaRepository<Games, Integer>{

}
