package com.ty.Sports_Club.Dto;

import java.time.LocalDate;
import java.util.List;

import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Events {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String event_Name;
	private LocalDate start_Date;
	private LocalDate end_Date;
	private double total_price;
	private String status;


	@ManyToOne
	@JoinColumn
	@JsonIgnore
	private User user;

	@OneToOne(mappedBy = "events")
	private EventPrice eventPrice;

	@OneToMany(mappedBy = "events1")
	private List<Games> games;

	public Events(int id, String event_Name, LocalDate start_Date, LocalDate end_Date, 
		 User user, EventPrice eventPrice, List<Games> games) {
		super();
		this.id = id;
		this.event_Name = event_Name;
		this.start_Date = start_Date;
		this.end_Date = end_Date;
		this.user = user;
		this.eventPrice = eventPrice;
		this.games = games;
	}

	public Events() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEvent_Name() {
		return event_Name;
	}

	public void setEvent_Name(String event_Name) {
		this.event_Name = event_Name;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public EventPrice getEventPrice() {
		return eventPrice;
	}

	public void setEventPrice(EventPrice eventPrice) {
		this.eventPrice = eventPrice;
	}

	public List<Games> getGames() {
		return games;
	}

	public void setGames(List<Games> games) {
		this.games = games;
	}

	public double getTotal_price() {
		return total_price;
	}

	public LocalDate getStart_Date() {
		return start_Date;
	}

	public void setStart_Date(LocalDate start_Date) {
		this.start_Date = start_Date;
	}

	public LocalDate getEnd_Date() {
		return end_Date;
	}

	public void setEnd_Date(LocalDate end_Date) {
		this.end_Date = end_Date;
	}

	public void setTotal_price(double total_price) {
		this.total_price = total_price;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
