package com.ty.Sports_Club.Dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class EventPrice {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private double per_Hour;
	private double per_Day;
	private double per_Month;

	@OneToOne
	@JoinColumn
	@JsonIgnore
	private Events events;

	public EventPrice(int id, double per_Hour, double per_Day, double per_Month, Events events) {
		super();
		this.id = id;
		this.per_Hour = per_Hour;
		this.per_Day = per_Day;
		this.per_Month = per_Month;
		this.events = events;
	}

	public EventPrice() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getPer_Hour() {
		return per_Hour;
	}

	public void setPer_Hour(double per_Hour) {
		this.per_Hour = per_Hour;
	}

	public double getPer_Day() {
		return per_Day;
	}

	public void setPer_Day(double per_Day) {
		this.per_Day = per_Day;
	}

	public double getPer_Month() {
		return per_Month;
	}

	public void setPer_Month(double per_Month) {
		this.per_Month = per_Month;
	}

	public Events getEvents() {
		return events;
	}

	public void setEvents(Events events) {
		this.events = events;
	}

}
