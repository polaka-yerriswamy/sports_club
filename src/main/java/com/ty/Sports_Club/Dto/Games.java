package com.ty.Sports_Club.Dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Games {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String game_Name;
	private double price;
	private int total_No_Of_Plots;
	private int available_Plots;

	@ManyToOne
	@JoinColumn
	private Events events1;

	

	public Games() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getGame_Name() {
		return game_Name;
	}

	public void setGame_Name(String game_Name) {
		this.game_Name = game_Name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Events getEvents1() {
		return events1;
	}

	public void setEvents1(Events events1) {
		this.events1 = events1;
	}

	public int getTotal_No_Of_Plots() {
		return total_No_Of_Plots;
	}

	public void setTotal_No_Of_Plots(int total_No_Of_Plots) {
		this.total_No_Of_Plots = total_No_Of_Plots;
	}

	public int getAvailable_Plots() {
		return available_Plots;
	}

	public void setAvailable_Plots(int available_Plots) {
		this.available_Plots = available_Plots;
	}

	
}
