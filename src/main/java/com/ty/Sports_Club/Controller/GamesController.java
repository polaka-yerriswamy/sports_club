package com.ty.Sports_Club.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.Sports_Club.Dto.Games;
import com.ty.Sports_Club.Services.GamesService;
import com.ty.Sports_Club.Utils.ResponseStructure;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@RestController
public class GamesController {

	@Autowired
	GamesService gamesService;
	
	@PostMapping("game")
	@ApiOperation("To Save the Game")
	@ApiResponses({@ApiResponse(code = 200, message = "Save  Game success"),
			@ApiResponse(code = 404, message = " Id not Find"),
			@ApiResponse(code = 500, message = "Internal Server error") })
	public ResponseEntity<ResponseStructure<Games>> saveGame( @RequestBody Games games) {
		return gamesService.saveGame(games);
	}
	
	@GetMapping("game/{id}")
	@ApiOperation("To Get the Game By Id")
	@ApiResponses({@ApiResponse(code = 200, message = "Data Retrived successfully"),
			@ApiResponse(code = 404, message = " Id not Find"),
			@ApiResponse(code = 500, message = "Internal Server error") })
	public ResponseEntity<ResponseStructure<Games>> getGameById(@PathVariable int id) {
		return gamesService.getGameById(id);
	}
	
	@GetMapping("game")
	@ApiOperation("To Get All Games")
	@ApiResponses({@ApiResponse(code = 200, message = "Data Retrived successfully"),
			@ApiResponse(code = 404, message = " Id not Find"),
			@ApiResponse(code = 500, message = "Internal Server error") })
	public ResponseEntity<ResponseStructure<List<Games>>> getAllGames() {
		return gamesService.getAllGames();
	}
	
	@PutMapping("game/{id}")
	@ApiOperation("To update the Game")
	@ApiResponses({@ApiResponse(code = 200, message = "Data updated successfully"),
			@ApiResponse(code = 404, message = " Id not Find"),
			@ApiResponse(code = 500, message = "Internal Server error") })
	public ResponseEntity<ResponseStructure<Games>> updateGame(@RequestBody Games games, @PathVariable int id) {
		return gamesService.updateGame(games, id);

	}
	
	@DeleteMapping("game")
	@ApiOperation("To Delete the Game")
	@ApiResponses({@ApiResponse(code = 200, message = "Data Deleted successfully"),
			@ApiResponse(code = 404, message = " Id not Find"),
			@ApiResponse(code = 500, message = "Internal Server error") })
	public ResponseEntity<ResponseStructure<String>> deleteGame(@RequestParam int id) {
		return gamesService.deleteGame(id);
	}
}
