package com.ty.Sports_Club.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ty.Sports_Club.Dto.Events;
import com.ty.Sports_Club.Services.EventsService;
import com.ty.Sports_Club.Utils.ResponseStructure;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class EventsController {

	@Autowired
	private EventsService eventsService;
	
	@PostMapping("/user/{userId}/events")
	@ApiOperation("To Save Events Based On User Id Data ")
	@ApiResponses({ @ApiResponse(code = 200, message = "Save Events Successfully"),
			@ApiResponse(code = 404, message = " Did not Find"),
			@ApiResponse(code = 500, message = "Internal Server error") })
	public ResponseEntity<ResponseStructure<Events>> saveEvents(@PathVariable int userId, @RequestBody Events events) {
		return eventsService.saveEvents(userId, events);
	}
	
	@GetMapping("/user/{userId}/events/{id}")
	@ApiOperation("To Get Events Based On Event Id Data ")
	@ApiResponses({ @ApiResponse(code = 200, message = "Fetch Event Successfully"),
			@ApiResponse(code = 404, message = " Did not Find"),
			@ApiResponse(code = 500, message = "Internal Server error") })
	public ResponseEntity<ResponseStructure<Events>> getEventsById(@PathVariable int id) {
		return eventsService.getEventsById(id);
	}
	
	@GetMapping("/events")
	@ApiOperation("To Get All Events  Data ")
	@ApiResponses({ @ApiResponse(code = 200, message = "Fetch All Events Successfully"),
			@ApiResponse(code = 404, message = " Did not Find"),
			@ApiResponse(code = 500, message = "Internal Server error") })
	public ResponseEntity<ResponseStructure<List<Events>>> getAllEvents() {
		return eventsService.getAllEvents();
	}
	
	@PutMapping("/user/{userId}/events/{id}")
	@ApiOperation("To Update Events Based On Event Id Data ")
	@ApiResponses({ @ApiResponse(code = 200, message = "Update Event Successfully"),
			@ApiResponse(code = 404, message = " Did not Find"),
			@ApiResponse(code = 500, message = "Internal Server error") })
	public ResponseEntity<ResponseStructure<Events>> updateEvent(@PathVariable int id, @RequestBody Events events) {
		return eventsService.updateEventsById(id, events);
	}
	
	@DeleteMapping("/user/{userId}/events/{id}")
	@ApiOperation("To Delete Event Based On Event Id Data ")
	@ApiResponses({ @ApiResponse(code = 200, message = "Delete Event Successfully"),
			@ApiResponse(code = 404, message = " Did not Find"),
			@ApiResponse(code = 500, message = "Internal Server error") })
	public ResponseEntity<ResponseStructure<String>> deleteEvent(@PathVariable int id) {
		return eventsService.deleteEvents(id);
	}
}
