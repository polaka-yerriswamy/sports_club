package com.ty.Sports_Club.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ty.Sports_Club.Dto.EventPrice;
import com.ty.Sports_Club.Services.EventPriceService;
import com.ty.Sports_Club.Utils.ResponseStructure;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class EventPriceController {

	@Autowired
	EventPriceService eventPriceService;

	@PostMapping("/eventPrice")
	@ApiOperation("To Save EventPrice Data")
	@ApiResponses({ @ApiResponse(code = 200, message = "Save  EventPrice success"),
			@ApiResponse(code = 404, message = " Id not Find"),
			@ApiResponse(code = 500, message = "Internal Server error") })
	public ResponseEntity<ResponseStructure<EventPrice>> saveEventPrice(@RequestBody EventPrice eventPrice) {
		return eventPriceService.saveEventPrice(eventPrice);
	}

	@GetMapping("/eventPrice")
	@ApiOperation("To Get All EventPrice Data")
	@ApiResponses({ @ApiResponse(code = 200, message = "Fetch All EventPrice Data success"),
			@ApiResponse(code = 404, message = " Id not Found"),
			@ApiResponse(code = 500, message = "Internal Server error") })
	public ResponseEntity<ResponseStructure<List<EventPrice>>> getAllEventPrice() {
		return eventPriceService.getAllEventPrice();
	}

	@GetMapping("/eventPrice/{id}")
	@ApiOperation("To Get eventPrice Data By Id")
	@ApiResponses({ @ApiResponse(code = 200, message = "Fetch Data success"),
			@ApiResponse(code = 404, message = " Id not Found"),
			@ApiResponse(code = 500, message = "Internal Server error") })
	public ResponseEntity<ResponseStructure<EventPrice>> getEventPriceById(@PathVariable int id) {
		return eventPriceService.getEventPriceById(id);
	}

	@PutMapping("/eventPrice/{id}")
	@ApiOperation("To Update eventPrice Data By Id")
	@ApiResponses({ @ApiResponse(code = 200, message = "Update eventPrice Data success"),
			@ApiResponse(code = 404, message = " Id not Found"),
			@ApiResponse(code = 500, message = "Internal Server error") })
	public ResponseEntity<ResponseStructure<EventPrice>> updateEventPrice(@PathVariable int id,
			@RequestBody EventPrice eventPrice) {
		return eventPriceService.updateEventPrice(id, eventPrice);
	}

	@DeleteMapping("/eventPrice/{id}")
	@ApiOperation("To Delete User Data By Id")
	@ApiResponses({ @ApiResponse(code = 200, message = "EventPrice Deleted"),
			@ApiResponse(code = 404, message = " Id not Found"),
			@ApiResponse(code = 500, message = "Internal Server error") })
	public ResponseEntity<ResponseStructure<String>> deleteUser(@PathVariable int id) {
		return eventPriceService.deleteEventPrice(id);
	}
}
