package com.ty.Sports_Club.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.Sports_Club.Dto.User;
import com.ty.Sports_Club.Services.UserServes;
import com.ty.Sports_Club.Utils.ResponseStructure;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
@RestController
public class UserController {
	@Autowired
	UserServes serves;
	@PostMapping("user")
	@ApiOperation("To save User")
	@ApiResponses({ @ApiResponse(code = 200, message = "User Saved"),
			@ApiResponse(code = 404, message = "Class not found"),
			@ApiResponse(code = 500, message = "Internal Server error") })
	public ResponseEntity<ResponseStructure<User>> saveuser(@RequestBody User user ) {
		return serves.saveUser(user);
	}
	@GetMapping("user")
	@ApiOperation("To get User")
	@ApiResponses({ @ApiResponse(code = 200, message =  "User are feathed sucessfully"),
			@ApiResponse(code = 404, message = "Class not found"),
			@ApiResponse(code = 500, message = "Internal Server error") })
	public ResponseEntity<ResponseStructure<List<User>>> getUser(){
		return serves.getUser();
	}
	@GetMapping("user/{id}")
	@ApiOperation("To get UserByID")
	@ApiResponses({ @ApiResponse(code = 200, message = "User are feathed by passing the Id"),
			@ApiResponse(code = 404, message = "Class not found"),
			@ApiResponse(code = 500, message = "Internal Server error") })
	public ResponseEntity<ResponseStructure<User>> getById(@PathVariable int id ){
		return serves.getbyid(id);
	}
	@PutMapping("user/{id}")
	@ApiOperation("To Update User")
	@ApiResponses({ @ApiResponse(code = 200, message = "User updated "),
			@ApiResponse(code = 404, message = "Class not found"),
			@ApiResponse(code = 500, message = "Internal Server error") })
	public ResponseEntity<ResponseStructure<User>> updateUser(@PathVariable int id,@RequestBody User user){
		return serves.update(user, id);
	}
	@DeleteMapping("user")
	@ApiOperation("To Delete user")
	@ApiResponses({ @ApiResponse(code = 200, message = "User Deleted"),
			@ApiResponse(code = 404, message = "Class not found"),
			@ApiResponse(code = 500, message = "Internal Server error") })
	public ResponseEntity<ResponseStructure<String>> deleteUser(@RequestParam int id){
		return serves.deleteUser(id);
	}

}
