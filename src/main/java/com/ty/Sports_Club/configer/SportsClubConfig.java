package com.ty.Sports_Club.configer;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SportsClubConfig {

	@Bean
	public Docket getDocket() {
		
		Contact contact=new Contact("Sports_Club","https://Sports_Club.com/","Braves@ty.com");
		
		List<VendorExtension> extensions=new ArrayList<VendorExtension>();
		
		ApiInfo apiInfo=new ApiInfo("Sports_Club Api Document",
				"Project is for booking plots from Users based on Hours", 
			     "TYP-Sports_Club-project-Sanpshot -1.0.1",
			     "https://Sports_Club.com/",
			     contact,
			     "license 1101", 
			     "https://Sports_Club.com/",
			     extensions);
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.ty.Sports_Club"))
				.build()
		        .apiInfo(apiInfo)
		        .useDefaultResponseMessages(false);
	}

}
