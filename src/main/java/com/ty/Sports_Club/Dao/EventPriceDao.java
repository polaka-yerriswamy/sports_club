package com.ty.Sports_Club.Dao;

import java.util.List;

import com.ty.Sports_Club.Dto.EventPrice;

public interface EventPriceDao {
	
	public EventPrice saveEventPrice(EventPrice eventPrice);
	
	public EventPrice getEventPriceById(int id);

	public List<EventPrice> getAllEventPrices();

	public EventPrice updateEventPrice(int id, EventPrice eventPrice);

	public boolean deleteEventPrice(int id);


}
