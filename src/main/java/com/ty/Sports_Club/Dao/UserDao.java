package com.ty.Sports_Club.Dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.ty.Sports_Club.Dto.User;

public interface UserDao {
	
	public User saveUser(User user);
	public List<User> getUser();
	public User getbyid(int id);
	public User update(User user,int id);
	public boolean deleteUser(int id);

}
