package com.ty.Sports_Club.Dao;

import java.util.List;

import com.ty.Sports_Club.Dto.Events;

public interface EventsDao {
	
	public Events saveEvents(int userId, Events events);
	
	public Events getEventsById(int id);

	public List<Events> getAllEvents();

	public Events updateEvent(int id, Events events);

	public boolean deleteEvents(int id);
}
