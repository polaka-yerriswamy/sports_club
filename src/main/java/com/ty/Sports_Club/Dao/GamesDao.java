package com.ty.Sports_Club.Dao;

import java.util.List;

import com.ty.Sports_Club.Dto.Games;

public interface GamesDao {

	public Games saveGame(Games games);
	
	public Games getGameById(int id);
	
	public List<Games> getAllGames();
	
	public Games updateGame(Games games,int id);
	
	public boolean deleteGame(int id);
}
