package com.ty.Sports_Club.Dao.Impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.Sports_Club.Dao.EventsDao;
import com.ty.Sports_Club.Dto.Events;
import com.ty.Sports_Club.Dto.User;
import com.ty.Sports_Club.Repositry.EventsRepository;
import com.ty.Sports_Club.Repositry.UserRepo;

@Repository
public class EventsDaoImpl implements EventsDao{
	
	@Autowired
	private EventsRepository eventsRepository;
	
	@Autowired
	private UserRepo userRepo;
	
	@Override
	public Events saveEvents(int userId, Events events) {
		User user = userRepo.getById(userId);
		events.setUser(user);
		return eventsRepository.save(events);
	}

	@Override
	public Events getEventsById(int id) {
		Optional<Events> optional = eventsRepository.findById(id);
		if (optional.isPresent()) {
			return optional.get();
		} else
			return null;
	}

	@Override
	public List<Events> getAllEvents() {
		return eventsRepository.findAll();
	}

	@Override
	public Events updateEvent(int id, Events events) {
		Events existingEvent = getEventsById(id);
		if(existingEvent !=null) {
			existingEvent.setEvent_Name(events.getEvent_Name());
			existingEvent.setEnd_Date(events.getEnd_Date());
			existingEvent.setEventPrice(events.getEventPrice());
			existingEvent.setStart_Date(events.getStart_Date());
			existingEvent.setGames(events.getGames());
			return eventsRepository.save(events);
		}else
			return null;
	}

	@Override
	public boolean deleteEvents(int id) {
		Events events = getEventsById(id);
		if (events != null) {
			eventsRepository.delete(events);
			return true;
		} else
			return false;
	}

}
