package com.ty.Sports_Club.Dao.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.Sports_Club.Dao.UserDao;
import com.ty.Sports_Club.Dto.User;
import com.ty.Sports_Club.Repositry.UserRepo;
@Repository
public class UserDaoImp implements UserDao {
	@Autowired
	UserRepo repo;
	@Override
	public User saveUser(User user) {
		
		return repo.save(user);
	}

	@Override
	public List<User> getUser() {
		return repo.findAll();
	}

	@Override
	public User getbyid(int id) {
		return repo.getById(id);
	}

	@Override
	public User update(User user,int id) {
		User exsisting=getbyid(id);
		exsisting.setName(user.getName());
		exsisting.setBranch(user.getBranch());
		exsisting.setEvents(user.getEvents());
		exsisting.setPhone_No(user.getPhone_No());
		exsisting.setRole(user.getRole());
		return repo.save(exsisting);
	}

	@Override
	public boolean deleteUser(int id) {
		User exsisting=getbyid(id);
		System.out.println(exsisting);
		if(exsisting!= null) {
			repo.delete(exsisting);
			return true;
		}
		return false;
	}

}
