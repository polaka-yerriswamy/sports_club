package com.ty.Sports_Club.Dao.Impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.Sports_Club.Dao.EventPriceDao;
import com.ty.Sports_Club.Dto.EventPrice;
import com.ty.Sports_Club.Repositry.EventPriceRepository;

@Repository
public class EventPriceDaoImpl implements EventPriceDao{
	
	@Autowired
	EventPriceRepository eventPriceRepository;

	@Override
	public EventPrice saveEventPrice(EventPrice eventPrice) {
		return eventPriceRepository.save(eventPrice);
	}

	@Override
	public EventPrice getEventPriceById(int id) {
		Optional<EventPrice> optional = eventPriceRepository.findById(id);
		if (optional.isPresent()) {
			return optional.get();
		}
		return null;
	}

	@Override
	public List<EventPrice> getAllEventPrices() {
		return eventPriceRepository.findAll();
	}

	@Override
	public EventPrice updateEventPrice(int id, EventPrice eventPrice) {
		EventPrice existingEventPrice = getEventPriceById(id);
		if (existingEventPrice != null) {
			existingEventPrice.setPer_Day(eventPrice.getPer_Day());
			existingEventPrice.setPer_Hour(eventPrice.getPer_Hour());
			existingEventPrice.setPer_Month(eventPrice.getPer_Month());
			return eventPriceRepository.save(existingEventPrice);
		}
		return null;
	}

	@Override
	public boolean deleteEventPrice(int id) {
		EventPrice eventPrice = getEventPriceById(id);
		if (eventPrice != null) {
			eventPriceRepository.delete(eventPrice);
			return true;
		} else
			return false;
	}

}
