package com.ty.Sports_Club.Dao.Impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.Sports_Club.Dao.GamesDao;
import com.ty.Sports_Club.Dto.Games;
import com.ty.Sports_Club.Repositry.GamesRepository;

@Repository
public class GamesDaoImpl implements GamesDao {

	@Autowired
	GamesRepository gamesRepository;
	
	@Override
	public Games saveGame(Games games) {
		return gamesRepository.save(games);
	}

	@Override
	public Games getGameById(int id) {
		Optional<Games> optional = gamesRepository.findById(id);
		if (optional.isPresent()) {
			return optional.get();
		}
		return null;
	}

	@Override
	public List<Games> getAllGames() {
		return gamesRepository.findAll();
	}

	@Override
	public Games updateGame(Games games, int id) {
		Games existingGame = getGameById(id);
		if (existingGame != null) {
			existingGame.setId(games.getId());
			existingGame.setGame_Name(games.getGame_Name());
			existingGame.setPrice(games.getPrice());
			existingGame.setEvents1(games.getEvents1());
			existingGame.setAvailable_Plots(games.getAvailable_Plots());;
			existingGame.setTotal_No_Of_Plots(games.getTotal_No_Of_Plots());
			return gamesRepository.save(existingGame);
		}
		return null;
	}

	@Override
	public boolean deleteGame(int id) {
		Games games = getGameById(id);
		if (games != null) {
			gamesRepository.delete(games);
			return true;
		}
		return false;
	}

}
