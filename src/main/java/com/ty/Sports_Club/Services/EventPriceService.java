package com.ty.Sports_Club.Services;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.ty.Sports_Club.Dto.EventPrice;
import com.ty.Sports_Club.Utils.ResponseStructure;

public interface EventPriceService {
	
	public ResponseEntity<ResponseStructure<EventPrice>> getEventPriceById(int id);

	public ResponseEntity<ResponseStructure<List<EventPrice>>> getAllEventPrice();

	public ResponseEntity<ResponseStructure<EventPrice>> updateEventPrice(int id, EventPrice eventPrice);

	public ResponseEntity<ResponseStructure<String>> deleteEventPrice(int id);

	public ResponseEntity<ResponseStructure<EventPrice>> saveEventPrice(EventPrice eventPrice);


}
