package com.ty.Sports_Club.Services;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.ty.Sports_Club.Dto.Events;
import com.ty.Sports_Club.Utils.ResponseStructure;

public interface EventsService {
	
	public ResponseEntity<ResponseStructure<Events>> saveEvents(int userId, Events events);
	
	public ResponseEntity<ResponseStructure<Events>> getEventsById(int id);
	
	public ResponseEntity<ResponseStructure<List<Events>>> getAllEvents();
	
	public ResponseEntity<ResponseStructure<Events>> updateEventsById(int id, Events events);
	
	public ResponseEntity<ResponseStructure<String>> deleteEvents(int id);
	
}
