package com.ty.Sports_Club.Services;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.ty.Sports_Club.Dto.Games;
import com.ty.Sports_Club.Utils.ResponseStructure;

public interface GamesService {

	public ResponseEntity<ResponseStructure<Games>> saveGame(Games games);

	public ResponseEntity<ResponseStructure<Games>> getGameById(int id);

	public ResponseEntity<ResponseStructure<List<Games>>> getAllGames();

	public ResponseEntity<ResponseStructure<Games>> updateGame(Games games,int id);

	public ResponseEntity<ResponseStructure<String>> deleteGame(int id);

}
