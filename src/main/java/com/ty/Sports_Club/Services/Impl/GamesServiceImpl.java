package com.ty.Sports_Club.Services.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ty.Sports_Club.Dao.GamesDao;
import com.ty.Sports_Club.Dto.Games;
import com.ty.Sports_Club.Services.GamesService;
import com.ty.Sports_Club.Utils.ResponseStructure;

@Service
public class GamesServiceImpl implements GamesService{

	@Autowired
	GamesDao gamesDao;
	
	@Override
	public ResponseEntity<ResponseStructure<Games>> saveGame(Games games) {
		ResponseStructure<Games> structure = new ResponseStructure<Games>();
		structure.setStatuscode(HttpStatus.OK.value());
		structure.setMessage("successful");
		structure.setData(gamesDao.saveGame(games));
		ResponseEntity<ResponseStructure<Games>> responseEntity = new ResponseEntity<ResponseStructure<Games>>(
				structure, HttpStatus.OK);
		return responseEntity;
	}

	@Override
	public ResponseEntity<ResponseStructure<Games>> getGameById(int id) {
		Games games = gamesDao.getGameById(id);

		if (games != null) {
			ResponseStructure<Games> structure = new ResponseStructure<Games>();
			structure.setStatuscode(HttpStatus.OK.value());
			structure.setMessage("successfull");
			structure.setData(games);
			ResponseEntity<ResponseStructure<Games>> responseEntity = new ResponseEntity<ResponseStructure<Games>>(
					structure, HttpStatus.OK);

			return responseEntity;
		} else {
			return null;
		}
	}

	@Override
	public ResponseEntity<ResponseStructure<List<Games>>> getAllGames() {
		List<Games> game = gamesDao.getAllGames();
		ResponseStructure<List<Games>> structure = new ResponseStructure<List<Games>>();
		if (game != null) {
			for (Games games : game) {
			games.setGame_Name(games.getGame_Name());
			}
			structure.setStatuscode(HttpStatus.OK.value());
			structure.setMessage("successful");
			structure.setData(game);
			ResponseEntity<ResponseStructure<List<Games>>> reponseEntity = new ResponseEntity<ResponseStructure<List<Games>>>(
					structure, HttpStatus.OK);

			return reponseEntity;
		} else {
			return null;
		}
	}

	@Override
	public ResponseEntity<ResponseStructure<Games>> updateGame(Games games, int id) {
		Games games2 = gamesDao.updateGame(games, id);
		if (games2 != null) {
			ResponseStructure<Games> structure = new ResponseStructure<Games>();
			structure.setStatuscode(HttpStatus.OK.value());
			structure.setMessage("successful");
			structure.setData(games2);
			ResponseEntity<ResponseStructure<Games>> responseEntity = new ResponseEntity<ResponseStructure<Games>>(
					structure, HttpStatus.OK);
			return responseEntity;

		} else {
			return null;
		}
	}

	@Override
	public ResponseEntity<ResponseStructure<String>> deleteGame(int id) {
		ResponseStructure<String> structure = new ResponseStructure<String>();
		ResponseEntity<ResponseStructure<String>> responseEntity;

		if (gamesDao.deleteGame(id)) {
			structure.setStatuscode(HttpStatus.OK.value());
			structure.setMessage("successful");
			structure.setData("Game deleted");
			responseEntity = new ResponseEntity<ResponseStructure<String>>(structure, HttpStatus.OK);
			return responseEntity;
		} else {
			return null;
		}
	}

}
