package com.ty.Sports_Club.Services.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ty.Sports_Club.Dao.EventsDao;
import com.ty.Sports_Club.Dto.Events;
import com.ty.Sports_Club.Services.EventsService;
import com.ty.Sports_Club.Utils.ResponseStructure;
import com.ty.Sports_Club.exception.IDNotFoundException;

@Service
public class EventsServiceImpl implements EventsService {

	@Autowired
	private EventsDao eventsDao;

	@Override
	public ResponseEntity<ResponseStructure<Events>> saveEvents(int userId, Events events) {
		ResponseStructure<Events> structure = new ResponseStructure<Events>();
		ResponseEntity<ResponseStructure<Events>> entity = null;
		structure.setStatuscode(HttpStatus.OK.value());
		structure.setMessage("success");
		structure.setData(eventsDao.saveEvents(userId, events));
		entity = new ResponseEntity<ResponseStructure<Events>>(structure, HttpStatus.OK);
		return entity;
	}

	@Override
	public ResponseEntity<ResponseStructure<Events>> getEventsById(int id) {
		ResponseStructure<Events> structure = new ResponseStructure<Events>();
		ResponseEntity<ResponseStructure<Events>> entity = null;
		Events events = eventsDao.getEventsById(id);
		if (events != null) {
			structure.setStatuscode(HttpStatus.OK.value());
			structure.setMessage("success");
			structure.setData(eventsDao.getEventsById(id));
			entity = new ResponseEntity<ResponseStructure<Events>>(structure, HttpStatus.OK);

		} else {
			throw new IDNotFoundException("Event ID :" + id + "does not exist");
		}
		return entity;
	}

	@Override
	public ResponseEntity<ResponseStructure<List<Events>>> getAllEvents() {
		ResponseStructure<List<Events>> structure = new ResponseStructure<List<Events>>();
		ResponseEntity<ResponseStructure<List<Events>>> entity = null;
		structure.setStatuscode(HttpStatus.OK.value());
		structure.setMessage("success");
		structure.setData(eventsDao.getAllEvents());
		entity = new ResponseEntity<ResponseStructure<List<Events>>>(structure, HttpStatus.OK);
		return entity;
	}

	@Override
	public ResponseEntity<ResponseStructure<Events>> updateEventsById(int id, Events events) {
		ResponseStructure<Events> structure = new ResponseStructure<Events>();
		ResponseEntity<ResponseStructure<Events>> entity = null;

		Events events2 = eventsDao.updateEvent(id, events);
		if (events2 != null) {
			structure.setStatuscode(HttpStatus.OK.value());
			structure.setMessage("success");
			structure.setData(events);
			entity = new ResponseEntity<ResponseStructure<Events>>(structure, HttpStatus.OK);
		} else {
			throw new IDNotFoundException("Event ID :" + id + "does not exist");
		}
		return entity;
	}

	@Override
	public ResponseEntity<ResponseStructure<String>> deleteEvents(int id) {
		ResponseStructure<String> responsestructure = new ResponseStructure<String>();
		ResponseEntity<ResponseStructure<String>> responseEntity = null;
		if (eventsDao.deleteEvents(id)) {
			responsestructure.setStatuscode(HttpStatus.OK.value());
			responsestructure.setMessage("Success");
			responsestructure.setData("Event deleted");
			responseEntity = new ResponseEntity<ResponseStructure<String>>(responsestructure, HttpStatus.OK);
			return responseEntity;
		} else {
			responsestructure.setStatuscode(HttpStatus.NOT_FOUND.value());
			responsestructure.setMessage("Id : " + id + "is not found");
			responsestructure.setData("No Event is deleted");
			responseEntity = new ResponseEntity<ResponseStructure<String>>(responsestructure, HttpStatus.NOT_FOUND);
			return responseEntity;
		}
	}

}
