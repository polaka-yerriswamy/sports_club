package com.ty.Sports_Club.Services.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ty.Sports_Club.Dao.UserDao;
import com.ty.Sports_Club.Dao.Impl.UserDaoImp;
import com.ty.Sports_Club.Dto.User;
import com.ty.Sports_Club.Services.UserServes;
import com.ty.Sports_Club.Utils.ResponseStructure;
import com.ty.Sports_Club.exception.IDNotFoundException;
@Service
public class UserServicesImpl implements UserServes{
	@Autowired
	UserDao userDao;
	@Override
	public ResponseEntity<ResponseStructure<User>> saveUser(User user) {
		ResponseEntity<ResponseStructure<User>> entity=null;
		ResponseStructure<User> structure=new ResponseStructure<User>();
		structure.setStatuscode(HttpStatus.OK.value());
		structure.setMessage("susefully Data stored");
		structure.setData(userDao.saveUser(user));
		return entity=new ResponseEntity<ResponseStructure<User>>(structure, HttpStatus.OK);
		
	}

	@Override
	public ResponseEntity<ResponseStructure<List<User>>> getUser() {
		ResponseEntity<ResponseStructure<List<User>>> entity=null;
		ResponseStructure<List<User>> structure=new ResponseStructure<List<User>>();
		structure.setStatuscode(HttpStatus.OK.value());
		structure.setMessage("retrived data");
		structure.setData(userDao.getUser());
		return entity=new ResponseEntity<ResponseStructure<List<User>>>(structure, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ResponseStructure<User>> getbyid(int id) {
		User existing=userDao.getbyid(id);
		ResponseEntity<ResponseStructure<User>> entity=null;
		ResponseStructure<User> structure=new ResponseStructure<User>();
		System.out.println(existing);
		if(existing!=null) {
			structure.setStatuscode(HttpStatus.OK.value());
			structure.setMessage("sucesfull value retrived");
			structure.setData(userDao.getbyid(id));
		return entity=new ResponseEntity<ResponseStructure<User>>(structure, HttpStatus.OK);
		}
		else {
			structure.setStatuscode(HttpStatus.NOT_FOUND.value());
			structure.setMessage("value for :"+id+"notfound");
			structure.setData(null);
		return entity=new ResponseEntity<ResponseStructure<User>>(structure, HttpStatus.NOT_FOUND);
		
		}
	}

	@Override
	public ResponseEntity<ResponseStructure<User>> update(User user, int id) {
		User existing=userDao.getbyid(id);
		ResponseEntity<ResponseStructure<User>> entity=null;
		ResponseStructure<User> structure=new ResponseStructure<User>();
		if(existing!=null) {
			structure.setStatuscode(HttpStatus.OK.value());
			structure.setMessage("sucesfull value retrived");
			structure.setData(userDao.update(user,id));
		return entity=new ResponseEntity<ResponseStructure<User>>(structure, HttpStatus.OK);
		}
		else {
			structure.setStatuscode(HttpStatus.NOT_FOUND.value());
			structure.setMessage("value for :"+id+"notfound");
			structure.setData(null);
		return entity=new ResponseEntity<ResponseStructure<User>>(structure, HttpStatus.NOT_FOUND);
		
		}
	}

	@Override
	public ResponseEntity<ResponseStructure<String>> deleteUser(int id) {
		boolean b=userDao.deleteUser(id);
		ResponseEntity<ResponseStructure<String>> entity=null;
		ResponseStructure<String> structure=new ResponseStructure<String>();
		System.out.println(b);
		if(b==true) {
			structure.setStatuscode(HttpStatus.OK.value());
			structure.setMessage("sucesfull ");
			structure.setData("user deleted ");
		return entity=new ResponseEntity<ResponseStructure<String>>(structure, HttpStatus.OK);
		}
		else {
			structure.setStatuscode(HttpStatus.NOT_FOUND.value());
			structure.setMessage("value for :"+id+"notfound");
			structure.setData("user not found with id:"+id);
		return entity=new ResponseEntity<ResponseStructure<String>>(structure, HttpStatus.NOT_FOUND);
		
		}
	}

}
