package com.ty.Sports_Club.Services.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ty.Sports_Club.Dao.EventPriceDao;
import com.ty.Sports_Club.Dto.EventPrice;
import com.ty.Sports_Club.Services.EventPriceService;
import com.ty.Sports_Club.Utils.ResponseStructure;
import com.ty.Sports_Club.exception.IDNotFoundException;

@Service
public class EventPriceServiceImpl implements EventPriceService {

	@Autowired
	EventPriceDao dao;

	@Override
	public ResponseEntity<ResponseStructure<EventPrice>> getEventPriceById(int id) {
		ResponseStructure<EventPrice> structure = new ResponseStructure<EventPrice>();
		ResponseEntity<ResponseStructure<EventPrice>> entity = null;
		EventPrice eventPrice = dao.getEventPriceById(id);
		if (eventPrice != null) {
			structure.setStatuscode(HttpStatus.OK.value());
			structure.setMessage("success");
			structure.setData(dao.getEventPriceById(id));
			entity = new ResponseEntity<ResponseStructure<EventPrice>>(structure, HttpStatus.OK);
		} else {
			throw new IDNotFoundException("EventPrice ID :" + id + "does not exist");
		}
		return entity;
	}

	@Override
	public ResponseEntity<ResponseStructure<List<EventPrice>>> getAllEventPrice() {
		ResponseStructure<List<EventPrice>> structure = new ResponseStructure<List<EventPrice>>();
		ResponseEntity<ResponseStructure<List<EventPrice>>> entity = null;
		structure.setStatuscode(HttpStatus.OK.value());
		structure.setMessage("success");
		structure.setData(dao.getAllEventPrices());
		entity = new ResponseEntity<ResponseStructure<List<EventPrice>>>(structure, HttpStatus.OK);
		return entity;

	}

	@Override
	public ResponseEntity<ResponseStructure<EventPrice>> updateEventPrice(int id, EventPrice eventPrice) {
		ResponseStructure<EventPrice> structure = new ResponseStructure<EventPrice>();
		ResponseEntity<ResponseStructure<EventPrice>> entity = null;

		EventPrice eventPrice2 = dao.updateEventPrice(id, eventPrice);
		if (eventPrice2 != null) {
			structure.setStatuscode(HttpStatus.OK.value());
			structure.setMessage("success");
			structure.setData(eventPrice);
			entity = new ResponseEntity<ResponseStructure<EventPrice>>(structure, HttpStatus.OK);
		} else {
			throw new IDNotFoundException("EventPrice ID :" + id + "does not exist");
		}
		return entity;
	}

	@Override
	public ResponseEntity<ResponseStructure<String>> deleteEventPrice(int id) {
		ResponseStructure<String> responsestructure = new ResponseStructure<String>();
		ResponseEntity<ResponseStructure<String>> responseEntity = null;
		if (dao.deleteEventPrice(id)) {
			responsestructure.setStatuscode(HttpStatus.OK.value());
			responsestructure.setMessage("Success");
			responsestructure.setData("Event Price deleted");
			responseEntity = new ResponseEntity<ResponseStructure<String>>(responsestructure, HttpStatus.OK);
			return responseEntity;
		} else {
			responsestructure.setStatuscode(HttpStatus.NOT_FOUND.value());
			responsestructure.setMessage("Id : " + id + "is not found");
			responsestructure.setData("No Item is deleted");
			responseEntity = new ResponseEntity<ResponseStructure<String>>(responsestructure, HttpStatus.NOT_FOUND);
			return responseEntity;
		}
	}

	@Override
	public ResponseEntity<ResponseStructure<EventPrice>> saveEventPrice(EventPrice eventPrice) {
		ResponseStructure<EventPrice> structure = new ResponseStructure<EventPrice>();
		ResponseEntity<ResponseStructure<EventPrice>> entity = null;
		structure.setStatuscode(HttpStatus.OK.value());
		structure.setMessage("success");
		structure.setData(dao.saveEventPrice(eventPrice));
		entity = new ResponseEntity<ResponseStructure<EventPrice>>(structure, HttpStatus.OK);
		return entity;
	}

}
