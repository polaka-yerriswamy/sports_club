package com.ty.Sports_Club.Services;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ty.Sports_Club.Dto.User;
import com.ty.Sports_Club.Utils.ResponseStructure;

public interface UserServes {
	
	public ResponseEntity<ResponseStructure<User>> saveUser(User user);
	public ResponseEntity<ResponseStructure<List<User>>> getUser();
	public ResponseEntity<ResponseStructure<User>> getbyid(int id);
	public ResponseEntity<ResponseStructure<User>> update(User user,int id);
	public ResponseEntity<ResponseStructure<String>> deleteUser(int id);


}
